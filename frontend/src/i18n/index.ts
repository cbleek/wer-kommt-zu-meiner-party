import enUS from './locales/en';
import deDE from './locales/de';
import frFR from './locales/fr';

export default {
  'en-US': enUS,
  'de-DE': deDE,
  'fr-FR': frFR,
};
