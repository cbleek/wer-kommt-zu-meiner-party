import { defineStore } from 'pinia';

export const useAuthStore = defineStore('auth', {
  state: () => ({
    token: null,
    user: {
      id: null,
      firstname: null,
      lastname: null,
      username: null,
      email: null,
      darkmode: 'auto',
      createdAt: null,
      updatedAt: null,
    },
    isLoggedIn: false,
    feedback: {
      confirmed: '',
      partyOnly: '',
      roomNeeded: '--',
      chat: ''
    }
  }),
  persist: true,
});
