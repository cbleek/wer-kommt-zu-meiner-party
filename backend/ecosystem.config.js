module.exports = {
  apps : [{
    name: '@wer-kommt-zu-meiner-party/backend',
    script: 'yarn',
    args: 'start',
    interpreter: '/bin/bash',
    env_production: {
      NODE_ENV: "production",
      PORT: 3002
    }
  }],
  
  deploy : {
    production: {
      user : 'yawik',
      host : 'api.yawik.org',
      ref  : 'origin/main',
      repo : 'https://gitlab.com/cbleek/wer-kommt-zu-meiner-party.git',
      path : '/home/yawik/wer-kommt-zu-meiner-party',
      'pre-deploy-local' : 'yarn && yarn build && rsync -a --delete build/ yawik@api.yawik.org:wer-kommt-zu-meiner-party/source/backend/build/',
      'post-deploy' : 'cd backend && yarn &&  pm2 startOrRestart ecosystem.config.js --interpreter bash --env production'
    },
  }
};
