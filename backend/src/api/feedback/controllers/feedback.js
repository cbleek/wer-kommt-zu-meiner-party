'use strict';

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::feedback.feedback', ({ strapi }) =>  ({
  async create(ctx) {
    let response = {}
    const userId = ctx.state.user.id;
    const feedback = await strapi.db.query("api::feedback.feedback").findOne({where: { user: userId}});

    ctx.request.body.data.user = userId;
    
    if (feedback) {
      ctx.params.id = feedback.id;
      response = await super.update(ctx).catch(err => { console.log(err) })
    } else {
      response = await super.create(ctx);
    }

    return response;
  },
  async my(ctx) {
    const feedback = await strapi.db.query("api::feedback.feedback").findOne({where: { user: ctx.state.user.id}});
    ctx.body = feedback;
  },
}));