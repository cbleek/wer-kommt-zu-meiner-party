# Wer kommt zu meiner Party?

Wer kennt nicht das Problem. Man möchte zu einer Party viele Leute einladen
und braucht das Feedback, ob die Auserwählten auch kommen. Damit man planen
kann.

- Bei einer Party kennen sich nicht alle Gäste. Es gibt Gäste, die nicht
  wollen, dass ein anderer Gast automatisch die private Email kennt.
- Alle Gäste wollen eigentlich wissen, wer da noch so kommt.

## Status 

WIP

## Features

Der Gastgeber soll durch Eingabe eines Namens und einer  Emailadrese einen
Gast einladen Können. Die Einladung geschieht durch den Versand eine Mail.
Der Gast wird namentlich angesprichen und bekommt einen individuellen Link,
der ihn authentifiziert auf eine Seite mit Fragen.

Der Gast beantwortet.

Der Gastgeber kann das Ergebnis einsehen.

## Requirements

- yarn

## Installation

```
git clone https://gitlab.com/cbleek/wer-kommt-zu-meiner-party.git party
cd party/backennd && yarn && cd ..
cd frondend && yarn && cd ..
```

----------

Dieses Tool soll helfen ....
